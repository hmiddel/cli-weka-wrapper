# CLI Weka Wrapper

A command line interface for using weka models on datasets in .arff format.

## Developing

To make use of this project, use the following command:

```shell
git clone https://bitbucket.org/hmiddel/cli-weka-wrapper.git
```

### Building

To build the project, use the gradle "fatJar" task. This will create a fully functional executable jar.

## Features

This project can process any arbitrary .model file exported from Weka, which it then uses on an ARFF file.

## Configuration

#### Help
Format: `-h`

Long Format: `--help`

Shows program help.

#### Input File
Type: `String`

Required: `Yes`

Format: `-i FILENAME`

Long Format: `--input_file FILE_NAME`

This argument specifies the input ARFF file to use.


#### Model
Type: `String`  

Required: `Yes`

Format: `-m FILENAME`

Long Format: `--model FILE_NAME`

This argument specifies what model should be used to process 

```bash
java -jar CLI-weka-wrapper.jar -i p53_protein.arff -m RandomForest.model #Loads in the "p53_protein.arff" file for processing, with the "RandomForest.model" model.
```

#### Output File
Type: `String`  

Required: `No`

Format: `-o FILENAME`

Long Format: `--output_file FILE_NAME`

This argument specifies to what file the program should output. If not specified, it will print to terminal.
Accepted formats are .arff and .csv.

```bash
java -jar CLI-weka-wrapper.jar -i p53_protein.arff -m RandomForest.model -o results.arff #Outputs to "results.arff"
```

#### Attribute selection
Type: `Array of integers`

Required: `No`

Format: `-a int1 int2 int3`

Long Format: `--attributes int1 int2 int3`

This argument specifies a number of attributes to use from the input dataset, instead of the full set of attributes.

## Contributing

If you'd like to contribute, please fork the repository and use a feature
branch. Pull requests are warmly welcome.

## Licensing

The code in this project is licensed under the GPLv3 license.
See `gpl.md`.