package nl.bioinf.hmiddel.CLI_weka_wrapper;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Creation date: Jun 28, 2017
 *
 * @version 0.01
 * @autor Michiel Noback (&copy; 2017)
 */
public class ApacheCliOptionsProviderTest {

    @Test(expected = java.lang.IllegalArgumentException.class)
    public void passIllegalArgsArray() throws Exception {
        String[] args = new String[]{"-t foo"};
        OptionsProvider op = new ApacheCliOptionsProvider(args);
    }

}