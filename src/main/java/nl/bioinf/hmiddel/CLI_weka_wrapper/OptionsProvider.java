/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.hmiddel.CLI_weka_wrapper;

import java.io.File;

/**
 * Interface containing the required options for the wrapper.
 * @author hmiddel
 */
public interface OptionsProvider {

    /**
     * The file to process with weka.
     * @return the file to process with weka.
     */
    File getFile();

    /**
     * The model file to use.
     * @return the model file to use.
     */
    File getModel();

    /**
     * The output file to use.
     * @return the output file to use.
     */
    File getOutFile();

    int[] getAttributes();
}
