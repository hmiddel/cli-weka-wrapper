/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.hmiddel.CLI_weka_wrapper;

import nl.bioinf.hmiddel.CLI_weka_wrapper.weka.WekaRunner;
import weka.core.Instances;

import java.io.IOException;
import java.util.Arrays;

/**
 * Main class designed to work with user input provided standard CL arguments and parsed using Apache CLI. Class is
 * final because it is not designed for extension.
 *
 * @author hmiddel
 */
public final class CommandLineArgsRunner {

    /**
     * private constructor because this class is not supposed to be instantiated.
     */
    private CommandLineArgsRunner() {
    }

    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        Instances classified;
        try {
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);
            if (op.helpRequested()) {
                op.printHelp();
                return;
            }
            if (!op.isParsed() || !op.getFile().exists()) {
                return;
            }
            WekaRunner runner = new WekaRunner();
            if (op.getModel() == null) {
                return;
            } else {
                classified = runner.start(op.getFile().getAbsolutePath(), op.getModel().getAbsolutePath(), op.getAttributes());
            }
            if (classified == null) {
                System.out.println("Something went wrong with classification.");
                return;
            }
            runner.printInstanceClasses(classified);
            if (op.getOutFile() != null) {
                runner.writeArff(op.getOutFile().getAbsolutePath(), classified);
            }
        } catch (IllegalStateException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(new String[]{});
            op.printHelp();
        } catch (IllegalArgumentException ex) {
            System.err.println("Illegal argument exception: " + ex.getMessage());
        } catch (IOException ex) {
            System.err.println("Something went wrong with file io:" + ex.getMessage());
        }
    }
}
