/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.hmiddel.CLI_weka_wrapper;

import org.apache.commons.cli.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * This class implements the OptionsProvider interface by parsing the passed command line arguments.
 *
 * @author hmiddel
 */
public class ApacheCliOptionsProvider implements OptionsProvider {
    private static final String HELP = "help";
    private static final String FILE = "input_file";
    private static final String OUT_FILE = "output_file";
    private static final String MODEL = "model";
    private static final String ATTRIBUTE_SELECTION = "attributes";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;
    private File file;
    private File model;
    private File out_file;
    private int[] attributes;

    /**
     * constructs with the command line array.
     *
     * @param args the CL array
     */
    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * check if help was requested; if so, return true.
     * @return helpRequested
     */
    public boolean helpRequested() {
        return commandLine != null && this.commandLine.hasOption(HELP);
    }


    /**
     * Check if the command line was successfully parsed.
     * @return is the command line parsed?
     */
    public boolean isParsed() {
        return this.commandLine != null;
    }

    /**
     * builds the Options object.
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message.");
        Option modelOption = new Option("m", MODEL, true, "The model to use.");
        Option fileOption = new Option("f", FILE, true, "The file to process.");
        Option outFileOption = new Option("o", OUT_FILE, true, "An optional output file.");
        Option attributeSelectionOption = new Option("a", ATTRIBUTE_SELECTION, true,
                "If the input data has more attributes than the model expects, this can be used to select an attribute set.");
        attributeSelectionOption.setArgs(Option.UNLIMITED_VALUES);
        fileOption.setRequired(true);
        modelOption.setRequired(true);
        options.addOption(helpOption);
        options.addOption(modelOption);
        options.addOption(fileOption);
        options.addOption(outFileOption);
        options.addOption(attributeSelectionOption);
    }

    /**
     * processes the command line arguments.
     */
    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            try {
                this.commandLine = parser.parse(this.options, this.clArguments);
            } catch (MissingOptionException ex) {
                printHelp();
                return;
            }
            String filelocation = commandLine.getOptionValue(FILE);
            if (!validateFile(filelocation, new String[] {"arff"})) {
                throw new IllegalArgumentException("Input file has an invalid extension.");
            }
            this.file = new File(filelocation);
            if (!file.exists()) {
                throw new FileNotFoundException("Input file not found: " + this.file.getPath());
            }
            String modellocation = commandLine.getOptionValue(MODEL);

            if (!validateFile(modellocation, new String[] {"model"})) {
                throw new IllegalArgumentException("Model file has an invalid extension.");
            }
            this.model = new File(modellocation);
            if (!model.exists()) {
                model = null;
                throw new FileNotFoundException("Model not found:" + modellocation + ", stopping execution.");
            }

            if (commandLine.hasOption(OUT_FILE)) {
                String out_file_location = commandLine.getOptionValue(OUT_FILE);
                this.out_file = new File(out_file_location);
                if (!validateFile(filelocation, new String[] {"arff","csv"})) {
                    throw new IllegalArgumentException("Output file has an invalid extension.");
                }
                if (out_file.exists()) {
                    throw new IOException("Can't override existing file: " + this.out_file.getPath());
                }
            }

            if (commandLine.hasOption(ATTRIBUTE_SELECTION)) {
                String[] attribute_input = commandLine.getOptionValues(ATTRIBUTE_SELECTION);
                int[] numeric_attributes = new int[attribute_input.length];
                try {
                    for (int i = 0; i < attribute_input.length; i++) {
                        numeric_attributes[i] = Integer.parseInt(attribute_input[i]);
                    }
                } catch(NumberFormatException ex) {
                    throw new IllegalArgumentException(ex);
                }
                this.attributes = numeric_attributes;
            }
        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex.getMessage());
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Checks if the specified file has a correct extension.
     * @param filelocation The file to check.
     * @param possible_extensions The extensions the file is allowed to have.
     * @return If the file has a correct extension.
     */
    private boolean validateFile(String filelocation, String[] possible_extensions) {
        String[] fileextension = filelocation.split("\\.");
        if (fileextension.length == 0) return false;
        for (String extension: possible_extensions) {
            if (fileextension[fileextension.length - 1].equals(extension)) {
                return true;
            }
        }
        return false;
    }

    /**
     * prints help.
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("CLI-weka-wrapper", options);
    }

    /**
     * See interface doc.
     * @return
     */
    @Override
    public File getFile() {
        return file;
    }

    /**
     * See interface doc.
     * @return
     */
    @Override
    public File getModel() {
        return model;
    }

    /**
     * See interface doc.
     * @return
     */
    @Override
    public File getOutFile() {
        return out_file;
    }

    @Override
    public int[] getAttributes() {
        return attributes;
    }

}
