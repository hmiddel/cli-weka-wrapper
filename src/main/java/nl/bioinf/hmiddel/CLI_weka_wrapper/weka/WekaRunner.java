package nl.bioinf.hmiddel.CLI_weka_wrapper.weka;

import weka.classifiers.AbstractClassifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

import java.io.IOException;


public class WekaRunner {

    /**
     * Start classification with weka.
     * @param unknownFile the file to perform classification on.
     * @param modelFile the model to use.
     * @param attributes the attributes to select. Can be null, in which case no selection while be done.
     */
    public Instances start(String unknownFile, String modelFile, int[] attributes) {
        try {
            AbstractClassifier fromFile = loadClassifier(modelFile);
            Instances unknownInstances = loadFile(unknownFile);
            if (attributes != null) {
                Remove remove = new Remove();
                remove.setInvertSelection(true);
                remove.setAttributeIndicesArray(attributes);
                remove.setInputFormat(unknownInstances);
                unknownInstances = Filter.useFilter(unknownInstances, remove);
            }
            return classifyInstances(fromFile, unknownInstances);
        } catch (Exception e) {
            System.out.println("Something went wrong with processing your input file with the specified model. Is the model made for this data?");
            System.out.println(e.getMessage());
            return null;
        }
    }


    /**
     * Classifies instances with a specific classifier.
     * @param classifier The classifier to use.
     * @param unknownInstances The instances to classify.
     * @return Classified instances.
     * @throws Exception
     */
    private Instances classifyInstances(AbstractClassifier classifier, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = classifier.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        return labeled;
    }


    /**
     * Loads a weka model file.
     * @param modelFile The file to load.
     * @return The model as an AbstractClassifier object.
     * @throws Exception
     */
    private AbstractClassifier loadClassifier(String modelFile) throws Exception {
        // deserialize model
        return (AbstractClassifier) weka.core.SerializationHelper.read(modelFile);
    }

    /**
     * Prints weka instance classes.
     * @param instances the instances to print.
     */
    public void printInstanceClasses(Instances instances) {
        int numInstances = instances.numInstances();
        for (int i = 0; i < numInstances; i++) {
            Instance instance = instances.instance(i);
            System.out.println(i + ": "  + instances.classAttribute().value((int)instance.classValue()));
        }
    }


    /**
     * Load a file using weka's built-in file loading.
     * @param datafile The file to load.
     * @return A list of instances from the file.
     * @throws IOException
     */
    private Instances loadFile(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }

    public void writeArff(String out_location, Instances instances) throws IOException {
        try {
            ConverterUtils.DataSink.write(out_location, instances);
        } catch (Exception e) {
            throw new IOException("Could not write to file");
        }
    }

}
